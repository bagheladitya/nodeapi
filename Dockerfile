FROM node:8
WORKDIR /app
COPY .  /app
CMD [ "npm" ,"install" ]
CMD [ "node", "app.js" ]